<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;

class EmployeeController extends Controller
{
    public function index()
    {
        //Show all employees from the database and return to view
        $employee = Employee::all();
        return view('list', ['employee'=>$employee]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * *@return \Illuminate\Http\Response
     **/
    public function create()
    {
        return view('create'); 
    }

     /**
     * Store a newly created resource in storage.
     *
     * *@param  \Illuminate\Http\Request  $request
     * *@return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = new Employee([
            'firstname' => $request->get('firstname'),
            'lastname' => $request->get('lastname'),
            'department' => $request->get('department'),
            'phone' => $request->get('phone'),
        ]);
        $employee->save();
        return redirect()->route('employees.index')->with('info', 'Employee Added Successfully');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        //Find Employee
        $employee = Employee::find($id);
        return view('edit', ['employee'=>$employee]);
    }
    public function detail($id){
        $employee = Employee::find($id);
        return view('detail', ['employee'=>$employee]);
    }

    //Update specified Employeee
    public function update(Request $request)
    {
        $employee = Employee::find($request->get('id'));
        $employee->firstname = $request->get('firstname');
        $employee->lastname = $request->get('lastname');
        $employee->department = $request->get('department');
        $employee->phone = $request->get('phone');
        $employee->save();
        return redirect()->route('employees.index')->with('info','Employee updated Successfully');
    }

    // Remove the specified resourse
    public function destroy($id)
    {
        $employee = Employee::find($id);
        //delete
        $employee->delete();
        return redirect()->route('employees.index')->with('info', 'Employee Deleted Successfully');
    }

}
