@extends('base')
@section('title','Employees Index')
@section('content')
<div>
    <a style="margin: 19px;" href="{{ route('employees.create')}}" class="btn btn-primary">New contact</a>
    </div>  
	<div class="row">
		<div class="col-sm-12">
			<table class="table">
				<tr>
					<th>ID</th>
					<th>First Name</th>
					<th>Last Name</th>
                    <th>Actions</th>
				</tr>
				@foreach($employee as $employee)
					<tr class = "text-center">
						<td>{{ $employee->id }}</td>
						<td>{{ $employee->firstname }}</td>
						<td>{{ $employee->lastname }}</td>
                        <td>
                            <a href="{{route('employees.edit',['id'=>$employee->id])}}" class = "btn btn-info">Edit</a>
                            <a href="{{route('employees.destroy',['id'=>$employee->id])}}" class = "btn btn-danger">Delete</a>
                            <a href="{{route('employees.detail',['id'=>$employee->id])}}" class = "btn btn-danger">Detail</a>
                        </td>
					</tr>
				@endforeach
			</table>
		</div>
	</div>
@endsection